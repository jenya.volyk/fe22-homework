// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
//     Создайте геттеры и сеттеры для этих свойств.
//     Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

// function Employee() {
//
//     this.getData = function () {
//         return [this.name, this.age, this.salary]
//     };
//     this.setData = function (name, age, salary) {
//         this.name = name;
//         this.age = age;
//         this.salary = salary;
//     }
// }
//
// function Programmer() {
//     Employee.apply(this, arguments)
// }
//
// Programmer.getData = function () {
//     return [this.name, this.age, this.salary * 3, this.lang]
// };
// Programmer.setData = function (lang) {
//     this.lang = lang
// };
// let programmerW = new Programmer();
// programmerW.setData('Klara', 22, 1200, 'C++');
// let programmerM = new Programmer();
// programmerM.setData('Steve', 35, 1000, 'java');
// let programmerG = new Programmer();
// programmerG.setData('Margarete', 18, 1800, 'Delphi');
// console.log(programmerW.getData());
// console.log(programmerM.getData());
// console.log(programmerG.getData());

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(newName) {
        this._name = newName;
    }

    get name() {
        return this._name;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get age() {
        return this._age;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }

    get salary() {
        return this._newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {

        super(name, age, salary);
        this.lang = lang
    }

    set salary(newSalary) {
        this._salary = newSalary * 3;
    }

    get salary() {
        return this._newSalary;
    }

    set lang(newLang) {
        this._lang = newLang;
    }

    get lang() {
        return this._lang;
    }
}

let programmerW = new Programmer('Klara', 22, 1200, 'C++');

let programmerM = new Programmer('Steve', 35, 1000, 'java');

let programmerG = new Programmer('Margarete', 18, 1800, 'Delphi');

console.log(programmerW);
console.log(programmerM);
console.log(programmerG);
