function gettingIP(url) {
    const buttonIp = document.getElementById('buttonIP');
    buttonIp.addEventListener('click', function () {
        getAddress(url)
    })

}

const container = document.getElementsByClassName('wrapper')[0];
const list = document.createElement('ul');
container.append(list);

async function getAddress(url) {
    const ip = await getIP(url);
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`);
    const data = await response.json();
   const data1=Object.entries(data);
    data1.forEach(item => {
        const listItem = document.createElement('li');
        listItem.innerHTML = item;
        list.appendChild(listItem)
    });
}

async function getIP(url) {
    const res = await fetch(url);
    const data = await res.json();
    return data.ip;
}

gettingIP('https://api.ipify.org/?format=json');

