const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
};

const {name, surname, age = 37, salary = 50000} = employee;
const newEmployee = {name, surname, age, salary};
console.log(newEmployee);