const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const clientsGeneral = [...clients1, ...clients2];
const uniqueClients= [...new Set (clientsGeneral)];

console.log(uniqueClients);
