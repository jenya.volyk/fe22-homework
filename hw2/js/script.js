const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

// const div = document.createElement('div');
// div.id = 'root';
// document.body.prepend(div);
//
// const list = document.createElement('ul');
// div.prepend(list);
//
// books.forEach(normalBook => {
//
//     try {
//
//         const {author, name, price} = normalBook;
//
//         if (author === undefined) {
//             throw new Error("данные неполны: нет автора");
//         }
//         if (name === undefined) {
//             throw new Error("данные неполны: нет названия");
//         }
//         if (price === undefined) {
//             throw new Error("данные неполны: нет цены");
//         }
//         const listElement = document.createElement('li');
//         listElement.innerHTML = Object.values({author, name, price});
//         list.append(listElement);
//     } catch (e) {
//         console.log("books error: " + e.message + ` '${normalBook.name}'`);
//     }
//
// });


const div = document.createElement('div');
div.id = 'root';
document.body.prepend(div);
const list = document.createElement('ul');
div.prepend(list);
books.forEach(normalBook => {
    try {
        const {author, name, price} = normalBook;
        checkError(author, name, price);
        const listElement = document.createElement('li');
        listElement.innerHTML = Object.values({author, name, price});
        list.append(listElement);
    } catch (e) {
        console.log("books error: " + e.message + ` '${normalBook.name}'`);
    }
});
function checkError(author, name, price) {
    if (author === undefined) {
        throw new Error("данные неполны: нет автора");
    }
    if (name === undefined) {
        throw new Error("данные неполны: нет названия");
    }
    if (price === undefined) {
        throw new Error("данные неполны: нет цены");
    }
}
