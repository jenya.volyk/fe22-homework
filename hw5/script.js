const wrapper = document.createElement('div');
const list = document.createElement('ul');

function renderFilm(film, id) {
    const listItem = document.createElement('li');
    const crawl = document.createElement('p');

    listItem.id = id;
    listItem.innerHTML = `${film.episode_id} ${film.title}`;

    list.appendChild(listItem);

    crawl.innerHTML = `${film.opening_crawl}`;
    listItem.appendChild(crawl)
}

// function getCharacters(links) {
//     return new Promise((resolve, reject) => {
//         const characters = [];
//         links.forEach(link => {
//             fetch(link)
//                 .then(response => response.json())
//                 .then(character => {
//                     characters.push(character.name);
//                     if (characters.length === links.length) {
//                         resolve(characters)
//                     }
//                 })
//         })
//     })
// }
function getCharacters(links) {
    return Promise.all(
        links.map(link => fetch(link)
            .then(response => response.json())
            .then(character => character.name)
        )
    ).then(characters => characters);
}
function renderCharacters(characters, id) {
    const episode = document.getElementById(id);
    const episodeInfo = episode.getElementsByTagName('p')[0];
    const charactersList = document.createElement('ul');
    episodeInfo.insertAdjacentElement('beforebegin', charactersList);
    characters.forEach(character => {
        const characterItem = document.createElement('li');
        characterItem.innerHTML = character;
        charactersList.appendChild(characterItem)
    })
}

fetch('https://swapi.dev/api/films/')
    .then((response) => {
        return response.json()
    })
    .then(films => {
        films.results.forEach(film => {
            const id = film.episode_id;
            renderFilm(film, id);
            getCharacters(film.characters)
                .then(charactersList => renderCharacters(charactersList, id))
        })

    });

document.body.appendChild(wrapper);
wrapper.appendChild(list);





