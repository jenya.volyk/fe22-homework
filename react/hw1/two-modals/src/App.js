import React from 'react';
import Button from "./components/Button/Button";
import Modals from "./components/Modals/Modals";
import './App.css'


class App extends React.Component {
    state = {
        modal: {
            isOpen: false,
            header: '',
            text: '',
        }

    };

    render() {
        const {modal} = this.state;

        return (
            <div className='app'>
                <Button
                    text="Open first modal"
                    onClick={() => this.setState({
                        modal: {
                            header: "Do you want to delete this file?",
                            text: "Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete?",
                            isOpen: true,
                        }
                    })}
                />
                <Button
                    text="Open second modal"
                    onClick={() => this.setState({
                        modal: {
                            header: "Second try?",
                            text: "Hello world",
                            isOpen: true,
                        }
                    })}
                />
                <Modals
                    header={modal.header}
                    cancelBtn={true}
                    text={modal.text}
                    onClose={() => this.setState({
                        modal: {
                            isOpen: false
                        }
                    })}
                    show={modal.isOpen}
                    actions={{
                        cancelButton: (onClose) => < button className='btns' onClick={onClose}>Cancel</button>,
                        okButton: (onClose) => <button className='btns' onClick={onClose}>Ok</button>
                    }}
                />
            </div>

        );
    }

}

export default App;