import React, {PureComponent} from 'react';
import './Modals.css'

export default class Modals extends PureComponent {
    onClose = e => {
        if (e.target === e.currentTarget) {
            this.props.onClose && this.props.onClose(e);
        }

    };

    render() {
        const {header, cancelBtn, text, actions} = this.props;
        if (!this.props.show) {
            return null;
        }
        return (
            <div className='modalBg' onClick={this.onClose}>
                <div className='modalBox'>
                    <div className='header'>
                        <h3>{header}</h3>
                        {cancelBtn && < div className='modalBtn' onClick={this.onClose}>&#10005;</div>}
                    </div>
                    <div className='modalDescription'>
                        <div className='modalTxt'>{text}</div>
                        <div className='btnBox'>
                            {actions.okButton(this.onClose)}
                            {actions.cancelButton(this.onClose)}
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}