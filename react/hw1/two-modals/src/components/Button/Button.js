import React, { Component } from 'react';
import './Button.css'


class Button extends Component {
    render() {
        const {text, onClick} = this.props;
        return (
            <button className="button" onClick={onClick}>
                {text}
            </button>
        );
    }
}

export default Button;